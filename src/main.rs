extern crate regex;

use regex::Regex;
use std::io;

static UNUSED:i32 = 0;
static WHITE:i32 = 1;
static BLACK:i32 = 2;
static EXIT:i32 = 17;
static ROWS:usize = 8;
static COLUMNS_PER_ROW:usize = 8;

fn main() {
    let mut board = vec![UNUSED; ROWS * COLUMNS_PER_ROW];
    let tl = Coordinate{x:3, y:3};
    let tr = Coordinate{x:4, y:3};
    let bl = Coordinate{x:3, y:4};
    let br = Coordinate{x:4, y:4};
    board[coord_to_idx(&tl) as usize] = BLACK;
    board[coord_to_idx(&tr) as usize] = WHITE;
    board[coord_to_idx(&bl) as usize] = WHITE;
    board[coord_to_idx(&br) as usize] = BLACK;

    main_loop(&mut board);
}

fn main_loop(mut board:&mut Vec<i32>) {
    let mut current_player = BLACK;
    print_round(&board, &current_player);

    loop {
        let mut in_buffer = String::new();
        if !try_read_input(&mut in_buffer) {
            return;
        }
        let t = handle_input(&mut board, &mut current_player, &in_buffer);
        if t == EXIT{
            break;
        }
    }
}

fn handle_input(mut board:&mut Vec<i32>, current_player :&mut i32, in_buffer:&str) -> i32 {
    let trimmed_input = in_buffer.trim();
    match trimmed_input.as_ref() {
        "exit" => return EXIT,
        "print" => print_board(board),
        _ => {
            if handle_board_input(&mut board, &trimmed_input, *current_player) {
                if *current_player == WHITE {
                    *current_player = BLACK;
                } else {
                    *current_player = WHITE
                }
            }

            print_round(&board, current_player);
        }
    }
    0
}

fn print_round(board:&Vec<i32>, current_player:&i32) {
    let player_name;
    if *current_player == WHITE {
        player_name = "white";
    } else {
        player_name = "black"
    }
    println!("current player is {}", player_name);
    print_board(board);
}

fn handle_board_input(mut board:&mut Vec<i32>, input:&str, player:i32) -> bool {
    let re = Regex::new(r"(\d) (\d)").unwrap();
    if re.is_match(input) {
        let captures = re.captures(input).unwrap();

        let x_text = captures.get(1).unwrap().as_str();
        let y_text = captures.get(2).unwrap().as_str();
        let x_tmp = x_text.parse::<i32>().unwrap();
        let y_tmp = y_text.parse::<i32>().unwrap();
        let my_pos = Coordinate{
            x: x_tmp,
            y: y_tmp,
        };

        if !check_and_flip_markers(my_pos, &mut board, player) {
            println!("You can not make that move");
            return false;
        }

        return true;
    } else {
        println!{"Failed to read player move. Write \"1 2\" for x = 1 and y = 2"};
        return false;
    }
}

fn check_and_flip_markers(position : Coordinate, mut board:&mut Vec<i32>, current_player:i32) -> bool {
    if board[coord_to_idx(&position) as usize] != UNUSED {
        return false;
    }

    let mut flipped = false;
    for i in -1..2 {
        for j in -1..2 {
            let direction = Direction { x: i, y: j };
            flipped = flipped || try_flip(&mut board, &position, &direction, &current_player);
        }
    }

    if flipped {
        board[coord_to_idx(&position) as usize] = current_player;
    }

    flipped
}

fn try_flip(mut board: &mut Vec<i32>, position: &Coordinate, direction: &Direction, current_color:&i32) -> bool {
    if check_possible_flip(&mut board, &position, &direction, &current_color) {
        flip(&mut board, &position, &direction, &current_color);
        return true;
    }
    return false;
}

fn flip(board: &mut Vec<i32>, position: &Coordinate, direction: &Direction, current_color:&i32) {
    if direction.x == 0 && direction.y == 0 {
        return;
    }

    let mut current_pos = coord_add(position, &direction);

    while coord_to_idx(&current_pos) < (ROWS*COLUMNS_PER_ROW)as i32 && coord_to_idx(&current_pos) >= 0 {
        board[coord_to_idx(&current_pos) as usize] = *current_color;

        if board[coord_to_idx(&current_pos) as usize] == UNUSED {
            break;
        }

        //found a friendly marker on the other side
        if board[coord_to_idx(&current_pos) as usize] == *current_color {
            break;
        }

        current_pos = coord_add(&current_pos, &direction);
    }
}

fn check_possible_flip(board: &mut Vec<i32>, position: &Coordinate, direction: &Direction, current_color:&i32) -> bool {
    if direction.x == 0 && direction.y == 0 {
        return false;
    }

    let mut possible_flip= false;
    let mut current_pos = coord_add(&coord_add(position, &direction), &direction);

    while coord_to_idx(&current_pos) < (ROWS*COLUMNS_PER_ROW)as i32 && coord_to_idx(&current_pos) >= 0 {
        if board[coord_to_idx(&current_pos) as usize] == UNUSED {
            break;
        }

        //found a friendly marker on the other side
        if board[coord_to_idx(&current_pos) as usize] == *current_color {
            possible_flip = true;
            break;
        }

        current_pos = coord_add(&current_pos, &direction);
    }
    possible_flip
}

fn try_read_input(mut in_buffer : &mut String) -> bool {
    match io::stdin().read_line(&mut in_buffer) {
        Ok(_n) => {
            return true;
        }
        Err(error) => {
            eprintln!("error: {}", error);
            return false;
        }
    }
}

fn print_board(board:&Vec<i32>) {
    let mut counter = 0;
    for cell in board.iter() {
        print_cell(*cell);
        if counter == (COLUMNS_PER_ROW - 1) as i32 {
            println!();
            counter = 0;
        } else {
            counter = counter + 1;
        }
    }
}

fn print_cell(cell:i32){
    if cell == UNUSED {
        print!("0 ")
    } else if cell == WHITE {
        print!("w ");
    } else if cell == BLACK {
        print!("b ");
    } else {
        print!("? ");
    }
}

fn coord_to_idx(cord:&Coordinate) -> i32 {
    cord.x + cord.y * (COLUMNS_PER_ROW as i32)
}

fn coord_add(a:&Coordinate, b:&Coordinate) -> Coordinate{
    Coordinate{ x: a.x + b.x, y : a.y + b.y}
}

struct Coordinate {
    x : i32,
    y : i32
}

type Direction = Coordinate;